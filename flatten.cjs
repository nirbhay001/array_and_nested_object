let resultData=[];
function flatten(elements){
    if (!Array.isArray(elements) && typeof elements!=='string'){
        return [];
    }
    for(let key in elements){
        let indexData=elements[key];
        if(Array.isArray(indexData)){
            flatten(indexData);
        }
        else{
            resultData.push(indexData);
        }
    }
return resultData;
}

module.exports=flatten;