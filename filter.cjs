function filter(elements, cb) {
    let newArray=[];
    if (!Array.isArray(elements) || typeof(cb)!= 'function') {
        return [];
    }
    for(let key=0;key<elements.length;key++){
        let value=cb(elements[key]);
        if(value===true){
            newArray.push(elements[key]);
        }
        else{
            continue;
        }
    }
    return newArray;

}

module.exports=filter;
