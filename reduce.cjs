function reduce(elements, callback, startingValue = null) {
    if (!Array.isArray(elements) || typeof (callback) != 'function' || elements.length === 0) {
        return undefined;
    }
    let temp = 0;
    if (startingValue === null) {
        startingValue = elements[0];
        temp++;
    }
    for (temp; temp < elements.length; temp++) {
        startingValue = callback(startingValue, elements[temp])
    }
    return startingValue;

}

module.exports = reduce;




