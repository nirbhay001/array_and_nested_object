function map(elements, callback){
    newArray=[];
    if (!Array.isArray(elements) || typeof callback !== "function") {
        return [];
    }
    for(let key=0;key<elements.length;key++){
        newArray.push(callback(elements[key], key));
    }
    return newArray;
}

module.exports=map;