function find(elements, cb) {
    let newarr=[];
    if (!Array.isArray(elements)|| elements.length===0 || typeof(cb)!= 'function') {
        return undefined;
    }
    for(let key=0;key<elements.length;key++){
        let value=cb(elements[key]);
        if(value===true){
            newarr.push(elements[key]);
        }
        else{
            continue;
        }
    }
    if(newarr.length===0){
        return undefined;
    }
    return newarr;

}

module.exports=find;

