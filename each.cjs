function each(elements, cb) {
    newArr=[];
    if (!Array.isArray(elements) ||  typeof(cb)!= 'function') {
        return elements;
    }
    else {
        for (let index = 0; index < elements.length; index++) {
            newArr.push(cb(elements[index]));
        }  
    }
    return newArr;
}
module.exports=each;
